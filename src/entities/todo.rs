use serde::{Deserialize, Serialize};

use super::Entity;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Todo {
    _id: String,
    version: u8,
    title: String,
    description: Option<String>,
    completed: bool,
}

impl Todo {
    pub fn new(id: String) -> Todo {
        Todo {
            _id: id,
            version: 0,
            title: String::new(),
            description: None,
            completed: false,
        }
    }

    pub fn set_version(&mut self, version: u8) {
        self.version = version
    }

    pub fn set_title(&mut self, title: String) {
        self.title = title
    }

    pub fn set_description(&mut self, description: String) {
        self.description = Some(description)
    }
}

impl Entity for Todo {
    const NAME: &'static str = "todo";

    fn id(&self) -> String {
        self._id.to_string()
    }

    fn version(&self) -> u8 {
        self.version.clone()
    }
}
