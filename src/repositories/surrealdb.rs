use async_trait::async_trait;
use surrealdb::{
    engine::remote::ws::{Client, Ws},
    opt::auth::Root,
    Surreal,
};

use super::Repository;
use crate::{config::Config, entities::Entity};

static CLIENT: Surreal<Client> = Surreal::init();

#[derive(Clone, Copy)]
pub struct SurrealDB {}

#[async_trait]
impl Repository for SurrealDB {
    async fn init(config: Config) -> Self {
        log::debug!(target: "repository", "SurrealDB init");

        CLIENT
            .connect::<Ws>(config.database.uri)
            .await
            .expect("Database URI");

        let username = config.database.username;
        let password = config.database.password;

        if username.is_some() && password.is_some() {
            CLIENT
                .signin(Root {
                    username: &username.expect("Database username"),
                    password: &password.expect("Database password"),
                })
                .await
                .expect("Database Auth");
        }

        CLIENT
            .use_ns(
                config
                    .database
                    .namespace
                    .expect("Database namespace")
                    .to_string(),
            )
            .use_db(config.database.name)
            .await
            .expect("Database namespace and name");

        SurrealDB {}
    }

    async fn find<T: Entity + Send + Sync>(self, id: String) -> Option<T> {
        log::debug!(target: "repository", "SurrealDB {:?} find id: {:?}", T::NAME, id);
        match CLIENT.select((T::NAME, &id)).await {
            Ok(found) => Some(found),
            Err(error) => {
                log::error!(target: "repository", "\t-> Error: {:?}", error);
                None
            }
        }
    }

    async fn upsert<T: Entity + Send + Sync>(self, entity: T) -> Result<(), ()> {
        log::debug!(target: "repository", "SurrealDB {:?} upsert: {:?}", T::NAME, entity);
        let _data: T = match CLIENT.update((T::NAME, &entity.id())).merge(entity).await {
            Ok(data) => data,
            Err(error) => {
                log::error!(target: "repository", "\t-> Error: {:?}", error);
                return Err(());
            }
        };
        Ok(())
    }

    async fn delete<T: Entity + Send + Sync>(self, id: String) -> Result<(), ()> {
        log::debug!(target: "repository", "SurrealDB {:?} delete id: {:?}", T::NAME, id);
        let _data: T = match CLIENT.delete((T::NAME, &id)).await {
            Ok(data) => data,
            Err(error) => {
                log::error!(target: "repository", "\t-> Error: {:?}", error);
                return Err(());
            }
        };
        Ok(())
    }
}
