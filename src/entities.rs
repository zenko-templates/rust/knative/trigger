pub mod todo;

use std::fmt;

use serde::{de::DeserializeOwned, Serialize};

pub trait Entity: DeserializeOwned + Serialize + Clone + fmt::Debug {
    const NAME: &'static str;

    fn id(&self) -> String;
    fn version(&self) -> u8;
}
