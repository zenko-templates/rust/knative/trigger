mod config;
mod entities;
mod filters;
mod handlers;
mod messages;
mod repositories;

use envconfig::Envconfig;
use std::net::Ipv4Addr;
use warp::Filter;

use config::{log::init_log, Config};

#[tokio::main]
async fn main() {
    let config = Config::init_from_env().unwrap();
    init_log(config.log_level.clone());

    let repository = repositories::create_from_config(config.clone()).await;

    let api = filters::main(config.clone(), repository.clone());

    let routes = api.with(warp::log("warp"));

    let host: Ipv4Addr = config.host.parse().unwrap();
    let port: u16 = config.port;

    warp::serve(routes).run((host, port)).await;
}
