use cloudevents::binding::warp::filter;
use warp::Filter;

use crate::{config::Config, handlers, repositories::Repository};

pub fn main(
    config: Config,
    repository: impl Repository,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    event(config.clone(), repository.clone())
}

pub fn event(
    config: Config,
    repository: impl Repository,
) -> impl Filter<Extract = (impl warp::Reply,), Error = warp::Rejection> + Clone {
    warp::post()
        .and(with_config(config))
        .and(with_repository(repository))
        .and(filter::to_event())
        .and_then(handlers::handle_event)
}

fn with_config(
    config: Config,
) -> impl Filter<Extract = (Config,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || config.clone())
}

fn with_repository(
    repository: impl Repository,
) -> impl Filter<Extract = (impl Repository,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || repository.clone())
}
