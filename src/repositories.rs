use async_trait::async_trait;

use crate::{
    config::{database::DatabaseType, Config},
    entities::Entity,
};

use self::surrealdb::SurrealDB;

pub mod surrealdb;

#[async_trait]
pub trait Repository: Clone + Copy + Send {
    async fn init(config: Config) -> Self;

    async fn find<T: Entity + Send + Sync>(self, id: String) -> Option<T>;

    async fn upsert<T: Entity + Send + Sync>(self, entity: T) -> Result<(), ()>;

    async fn delete<T: Entity + Send + Sync>(self, id: String) -> Result<(), ()>;
}

pub async fn create_from_config(config: Config) -> impl Repository {
    match config.database.ty {
        DatabaseType::SurrealDb => SurrealDB::init(config.clone()).await,
    }
}
