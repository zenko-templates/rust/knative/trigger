use cloudevents::{AttributesReader, Event};
use std::convert::Infallible;
use warp::hyper::StatusCode;

use crate::{config::Config, entities::todo::Todo, messages::Message, repositories::Repository};

pub async fn handle_event(
    _config: Config,
    repository: impl Repository,
    event: Event,
) -> Result<impl warp::Reply, Infallible> {
    log::debug!(target: "handler", "Event {:?} Received", &event.ty());

    let id = match event.extension("partitionkey") {
        Some(id) => id.to_string(),
        None => String::new(),
    };

    log::debug!(target: "handler", "\t-> Partition key: {:?}", id);

    let data: Message = match event.data() {
        Some(data) => match data.clone().try_into() {
            Ok(data) => data,
            Err(error) => {
                log::error!(target: "handler", "\t->Cannot parse message: {:?}", error);
                return Ok(StatusCode::BAD_REQUEST);
            }
        },
        None => {
            log::error!(target: "handler", "\t->No message given");
            return Ok(StatusCode::BAD_REQUEST);
        }
    };

    log::debug!(target: "handler", "\t-> Data: {:?}", data);

    // TODO: Add the handler logic
    let mut todo = Todo::new(data.id);
    todo.set_version(data.version);
    todo.set_title(data.title);
    if let Some(description) = data.description {
        todo.set_description(description);
    }

    log::debug!(target: "handler", "\t-> Todo: {:?}", &todo);

    match repository.upsert(todo.clone()).await {
        Ok(_) => Ok(StatusCode::NO_CONTENT),
        Err(_) => {
            log::error!(target: "handler", "\t Error when upserting the todo: {:?}", todo);
            Ok(StatusCode::INTERNAL_SERVER_ERROR)
        }
    }
}
