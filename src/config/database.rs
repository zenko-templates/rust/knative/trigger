use envconfig::Envconfig;
use std::str::FromStr;

#[derive(Clone)]
pub enum DatabaseType {
    SurrealDb,
}

impl FromStr for DatabaseType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.trim().to_lowercase().as_ref() {
            "surrealdb" => Ok(DatabaseType::SurrealDb),
            _ => Err(format!("Unknown database type: {s}")),
        }
    }
}

#[derive(Envconfig, Clone)]
pub struct Database {
    #[envconfig(from = "DATABASE_TYPE")]
    pub ty: DatabaseType,
    #[envconfig(from = "DATABASE_URI", default = "127.0.0.1:8000")]
    pub uri: String,

    #[envconfig(from = "DATABASE_USERNAME")]
    pub username: Option<String>,
    #[envconfig(from = "DATABASE_PASSWORD")]
    pub password: Option<String>,

    #[envconfig(from = "DATABASE_NAMESPACE")]
    pub namespace: Option<String>,
    #[envconfig(from = "DATABASE_NAME")]
    pub name: String,
}
