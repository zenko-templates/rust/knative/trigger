use cloudevents::Data;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct Message {
    pub id: String,
    pub title: String,
    pub description: Option<String>,
    pub version: u8,
}

impl TryFrom<Data> for Message {
    type Error = ();

    fn try_from(value: Data) -> Result<Self, Self::Error> {
        let serde_value: serde_json::Value = match value.try_into() {
            Ok(serde_value) => serde_value,
            Err(_) => return Err(()),
        };
        match serde_json::from_value(serde_value) {
            Ok(data) => Ok(data),
            Err(_) => Err(()),
        }
    }
}
