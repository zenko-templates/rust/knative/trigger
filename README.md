# Knative Rust Trigger Starter

A starter to generate Knative trigger using rust and warp

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Deploy](#deploy)

## Installation

Clone the project

```sh
git clone git@gitlab.com:zenko-templates/rust/knative/trigger.git
```

Update the files:
- [ ] The name in the [k8s service file](k8s/service.yaml)
- [ ] The name in the [k8s trigger file](k8s/trigger.yaml)

Remove the boilerplate exemple: (Optional)
- [ ] The file [todo.rs](src/entities/todo.rs)
- [ ] The structure message inside [messages.rs](src/messages.rs) 
- [ ] The logic inside [handlers.rs](src/handlers.rs) 

Create a new repository

```sh
rm -rf .git
git init
git add .
git commit -m "initial commit"
git remote add origin git@gitlab.com:
git push --all
```

## Usage

To handle a event you only need to modify three files:
- [ ] [handlers.rs](src/handlers.rs), it contains all the logic
- [ ] [messages.rs](src/messages.rs), it contains the structure of the event data
- [ ] Create one or multiple files in [entities folder](src/entities/) to represent a object in database

To use one of the database you can start it using the docker compose command:
```sh
docker-compose up surrealdb
```
Each database will be set to use the username `root` and the password `root` by default in the [docker compose file](docker-compose.yml)


## Deploy

Update the config file:
- [ ] The environment variable in the [k8s service file](k8s/service.yaml)
- [ ] The filters in the [k8s trigger file](k8s/trigger.yaml)

```sh
kube apply -f k8s/service.yaml
kube apply -f k8s/trigger.yaml
```

